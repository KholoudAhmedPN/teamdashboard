@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <label for="exampleInputEmail1">Company ID : </label>
                        <label>{{$ticket->company_id}}</label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description : </label>
                        <label>{{$ticket->description}}</label>

                    </div>
                    <div class="form-group">
                        <label for="email">User mail :</label>
                        <label>{{$ticket->email}}</label>

                    </div>

            </div>
        </div>
    </div>
</div>
@endsection
