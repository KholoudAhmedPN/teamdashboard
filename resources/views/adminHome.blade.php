@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <label>Admin</label>
            <div class="container">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Email</th>
                            <th scope="col">Company ID</th>
                            <th scope="col">View Ticket</th>
                            <th scope="col">Mark as Read</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($tickets as $ticket)
                              <tr>
                                  <td>{{$ticket->id}}</td>
                                  <td>{{$ticket->email}}</td>
                                  <td>{{$ticket->company_id}}</td>
                                  <td><a class="btn btn-primary" href="{{ url('view-ticket/'.$ticket->id) }}">View </a></td>
                                  <td>
                                    <form method="POST" action="{{route('mark-ticket')}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="id" value="{{$ticket->id}}" />
                                    @if($ticket->status == 0 ||$ticket->status == NULL ) <button  class="btn btn-primary" type="submit">Mark</button>@else  <label>Seen </label>@endif
                                    </form>
                                  </td>

                              </tr>
                          @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
    </div>
</div>
@endsection
