<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function adminHome()
    {
        $tickets = Ticket::all();
        return view('adminHome',compact('tickets'));
    }

    public function submitTicket(Request $request)
    {
         $request->validate([
            'company_id' => 'required|numeric',
            'email'=>'required|email',
            'description' => 'required',
        ]);

        Ticket::create($request->all());

        return  back()->with('message', 'Ticket has been saved successfully');

    }

    public function markTicket(Request $request)
    {
        $ticket = Ticket::find($request->id);

        $ticket->status = 1;

        $ticket->save();

        return  back()->with('message', 'Status has been saved successfully');
    }

    public function viewTicket($ticket)
    {
        $ticket = Ticket::find($ticket);

        return view('viewTicket',compact('ticket'));
    }
}
